#include<dirent.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>

int idx;
char buff[PATH_MAX];		//buffer
int ok;
char initialPath[PATH_MAX];	
	
int treeDirectory(char *path){
	//DIR *directory;	//pt gesionarea continutului unui director folosim tipul DIR
	DIR *directory;

	//struct dirent *directoryEntry;	//pt gestionarea unei intrari folosim struct dirent; pt nume folosim d_name, pt tip d_type
	struct dirent *directoryEntry;

	if(idx == 0 && ok == 0) {
		strcpy(initialPath,  getcwd(buff,sizeof(buff)));
		ok = 1;
	}

	directory = opendir(path);	//open directory
	if(directory == NULL) {	//opendir returns NULL if couldn't open directory
		perror(path);return 1;
	}	

	int changedDirSuccessful = chdir(path);
	if(changedDirSuccessful == -1) {
		perror(path);
		closedir(directory);
		return 1;
	}	

	directoryEntry = readdir(directory);	
	while(directoryEntry != NULL) {
		if( strcmp(directoryEntry->d_name,".") != 0 && strcmp(directoryEntry->d_name,"..")!= 0 ){
			for(int i=0; i < idx; i++) {
				printf("  ");
			}
			printf("|--%s\n",directoryEntry->d_name);
		}
		if(directoryEntry->d_type == DT_DIR && strcmp(directoryEntry->d_name,".") != 0 && strcmp(directoryEntry->d_name,"..") != 0) {	//daca  e director
			idx++;
			char newPath[PATH_MAX];
			strcpy(newPath, getcwd(buff,sizeof(buff)));				
			if(strlen(newPath) >= 1 && newPath[strlen(newPath)-1] != '/') {
				strcat(newPath, "/");
			}
			strcat(newPath, directoryEntry->d_name);	
			treeDirectory(newPath);
			idx--;
		}
		directoryEntry = readdir(directory);	
	}
	chdir("..");
	if(idx ==0 && ok == 1) { 
		chdir(initialPath);
		printf("cale initiala %s\n",getcwd(buff,sizeof(buff)));
		ok = 0;
	}
}
int main(int argc, char **argv) {
	if(argc != 2){
		fprintf(stderr,"Utilizare %s: director\n",argv[0]);
		return 1;
	}	
	if(strcmp(argv[1],"") == 0) {		//daca e string gol
		fprintf(stderr,"Invalid path\n");
		return 1;
	}
	printf("Tree for PATH %s\n",argv[1]);	
	treeDirectory(argv[1]);
	return 0;
}
